#ifndef __MPDPP_MPD_H
#define __MPDPP_MPD_H

struct mpd_connection;
struct mpd_song;
struct mpd_status;

namespace mpd {

enum class Tag {
    Unknown = -1,
    Artist,
    Album,
    AlbumArtist,
    Title,
    Track,
    Name,
    Genre,
    Date,
    Composer,
    Performer,
    Comment,
    Disc,
    MusicBrainz_ArtistID,
    MusicBrainz_AlbumID,
    MusicBrainz_AlbumArtistID,
    MusicBrainz_TrackID,
    MusicBrainz_ReleaseTrackID,
    OriginalDate,
    ArtistSort,
    AlbumArtistSort,
    AlbumSort,
    Count
};

class Song {
   public:
    Song(struct mpd_song* s);
    ~Song();

    const char* getTag(Tag tag);

   private:
    struct mpd_song* m_song;
};

class Status {
   public:
    Status(struct mpd_status* s);
    ~Status();

    bool isPlaying();

   private:
    struct mpd_status* m_status;
};

class Connection {
   public:
    Connection(const char* host, int port = 0, int timeout = 0);
    ~Connection();

    Song getCurrentSong();

    Status getStatus();

    bool playPause();
    bool prev();
    bool next();

   private:
    struct mpd_connection* m_conn;
};

};  // namespace mpd

#endif
