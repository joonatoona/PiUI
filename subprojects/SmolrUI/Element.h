#ifndef __SMOLRUI_ELEMENT_H
#define __SMOLRUI_ELEMENT_H

#include <SFML/Graphics.hpp>
#include <functional>

namespace sui {

class Element {
   public:
    virtual ~Element(){};

    virtual void onClick();
    virtual void render(sf::RenderWindow& win) = 0;

    virtual void setPosition(int x, int y) = 0;
    void setClickCallback(const std::function<void()>& callback);

    virtual const sf::FloatRect getBounds() const = 0;

   private:
    const std::function<void()>* m_callback = nullptr;
};

};  // namespace sui

#endif
