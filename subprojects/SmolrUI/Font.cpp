#include "Font.h"

#include "FontFile.otf.h"

namespace sui {

sf::Font* getFont() {
    static sf::Font* instance = new sf::Font;
    static bool loaded = false;

    if (!loaded) {
        instance->loadFromMemory(FontFile_otf, FontFile_otf_len);
        loaded = true;
    }

    return instance;
}

};  // namespace sui
