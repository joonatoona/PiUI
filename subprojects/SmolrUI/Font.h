#ifndef ___SMOLRUI_FONT_H
#define ___SMOLRUI_FONT_H

#include <SFML/Graphics.hpp>
#include <cassert>

namespace sui {

sf::Font* getFont();

}  // namespace sui

#endif
