#include "Icon.h"

namespace sui {

Icon::Icon(const sf::Texture &tx, sf::IntRect ir) {
    m_sprite.setTexture(tx);
    m_sprite.setTextureRect(ir);
}

void Icon::setTexture(const sf::Texture &tx, sf::IntRect ir) {
    m_sprite.setTexture(tx);
    m_sprite.setTextureRect(ir);
}

void Icon::setScale(int x, int y) { m_sprite.setScale(x, y); }
void Icon::setColor(sf::Color c) { m_sprite.setColor(c); }

void Icon::render(sf::RenderWindow &win) { win.draw(m_sprite); }
void Icon::setPosition(int x, int y) { m_sprite.setPosition(x, y); }

const sf::FloatRect Icon::getBounds() const {
    return m_sprite.getGlobalBounds();
}

};  // namespace sui
