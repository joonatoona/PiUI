#ifndef __ICONSPRITE_H
#define __ICONSPRITE_H

#include <Element.h>

namespace sui {

class Icon : public Element {
   public:
    Icon(const sf::Texture &tx, sf::IntRect ir);

    void setTexture(const sf::Texture &tx, sf::IntRect ir);

    void setColor(sf::Color c);
    void setScale(int x, int y);

    virtual void render(sf::RenderWindow &win);

    virtual void setPosition(int x, int y);

    virtual const sf::FloatRect getBounds() const;

   private:
    sf::Sprite m_sprite;
};

}  // namespace sui

#endif
