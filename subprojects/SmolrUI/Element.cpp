#include "Element.h"

namespace sui {

void Element::onClick() {
    if (m_callback) (*m_callback)();
}

void Element::setClickCallback(const std::function<void()>& callback) {
    m_callback = new std::function<void()>(callback);
}

};  // namespace sui
