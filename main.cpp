#include <Button.h>
#include <Font.h>
#include <Icon.h>
#include <Window.h>
#include <mpd.h>
#include <ctime>
#include <iostream>
#include "Icons/sheet.h"

sui::Icon makeIcon(sf::IntRect icon) {
    sui::Icon i = sui::Icon(*getSheet(), icon);
    i.setColor(sf::Color::Green);
    i.setScale(4, 4);
    return i;
}

std::string s_strftime(const char* fmt) {
    time_t current_time;
    struct tm* time_info;
    char timeString[128];

    time(&current_time);
    time_info = localtime(&current_time);

    strftime(timeString, 128, fmt, time_info);

    return std::string(timeString);
}

class MainWindow {
   public:
    MainWindow() : m_c("127.0.0.1") {
        setupTitlebar();
        setupLauncher();
        setupMusic();
    }

    void setupLauncher() {
        sf::Vector2u s = m_win.getSize();
        s.y += 62;

        m_musicButton.setPosition(s.x / 4 - 36, s.y / 2 - 36);
        m_musicButton.setClickCallback([&]() { m_win.setScene("music"); });

        m_infoButton.setPosition(s.x / 4 * 3 - 36, s.y / 2 - 36);
        m_infoButton.setClickCallback([&]() { m_win.setScene("info"); });

        m_win.addElement(&m_musicButton);
        m_win.addElement(&m_infoButton);

        addTitlebar("main");
    }

    void setupMusic() {
        sf::Vector2u s = m_win.getSize();
        s.y += 62;

        m_win.setInterval(std::bind(&MainWindow::updateMusic, this), 100);

        m_win.addElement(&m_musicTrack, "music");
        m_win.addElement(&m_musicArtist, "music");

        m_prevButton.setPosition(s.x / 7 - 36, s.y / 2);
        m_playPauseButton.setPosition(s.x / 6 * 3 - 36, s.y / 2);
        m_nextButton.setPosition(s.x / 7 * 6 - 36, s.y / 2);

        m_prevButton.setClickCallback([&]() { m_c.prev(); });
        m_playPauseButton.setClickCallback([&]() { m_c.playPause(); });
        m_nextButton.setClickCallback([&]() { m_c.next(); });

        m_win.addElement(&m_prevButton, "music");
        m_win.addElement(&m_playPauseButton, "music");
        m_win.addElement(&m_nextButton, "music");

        addTitlebar("music");
    }

    void setupTitlebar() {
        sf::Vector2u s = m_win.getSize();

        m_homeIcon.setScale(2, 2);
        m_homeButton.setChild(&m_homeIcon);
        m_homeButton.setPosition(s.x - m_homeButton.getBounds().width - 10, 10);
        m_homeButton.setClickCallback([&]() { m_win.setScene("main"); });

        m_clock.setPosition(10, 20);

        m_win.setInterval([&]() { m_clock.setText(s_strftime("%H:%M:%S")); },
                          1000);

        addTitlebar("info");
    }

    void addTitlebar(const std::string& scene) {
        m_win.addElement(&m_homeButton, scene);
        m_win.addElement(&m_clock, scene);
    }

    void run() { m_win.run(); }

    void updateMusic() {
        std::string title, artist;
        bool playing;

        try {
            mpd::Song s = m_c.getCurrentSong();
            title = s.getTag(mpd::Tag::Title);
            artist = s.getTag(mpd::Tag::Artist);
            playing = m_c.getStatus().isPlaying();
        } catch (std::exception) {
            title = "CONNECTION ERROR";
            artist = "Your internet";
            playing = false;
        }

        m_musicTrack.setText(title);
        m_musicArtist.setText(artist);

        m_musicTrack.setPosition(320 / 2 - (m_musicTrack.getBounds().width / 2),
                                 72);
        m_musicArtist.setPosition(
            320 / 2 - (m_musicArtist.getBounds().width / 2), 92);

        m_playPauseButton.setChild(playing ? &m_pauseIcon : &m_playIcon);
    }

   private:
    mpd::Connection m_c;
    sui::Window m_win;

    // ICONS
    sui::Icon m_homeIcon = makeIcon(ICON_HOME);
    sui::Icon m_infoIcon = makeIcon(ICON_INFO);
    sui::Icon m_musicIcon = makeIcon(ICON_MUSIC);
    sui::Icon m_nextIcon = makeIcon(ICON_NEXT);
    sui::Icon m_pauseIcon = makeIcon(ICON_PAUSE);
    sui::Icon m_playIcon = makeIcon(ICON_PLAY);
    sui::Icon m_prevIcon = makeIcon(ICON_PREV);

    // TITELBAR
    sui::Button m_homeButton = sui::Button(&m_homeIcon);
    sui::Label m_clock;

    // APP LAUNCHERS
    sui::Button m_musicButton = sui::Button(&m_musicIcon);
    sui::Button m_infoButton = sui::Button(&m_infoIcon);

    // MUSIC PLAYER
    sui::Label m_musicTrack;
    sui::Label m_musicArtist;
    sui::Button m_prevButton = sui::Button(&m_prevIcon);
    sui::Button m_playPauseButton = sui::Button(&m_playIcon);
    sui::Button m_nextButton = sui::Button(&m_nextIcon);
};

int main(int argc, char* argv[]) {
    MainWindow w;
    w.run();
}
